# This file is part of Weatherline.
#
# Weatherline is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Weatherline is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Weatherline.  If not, see <https://www.gnu.org/licenses/>.

import fontawesome as fa
import json
import os
from time import time

from pyowm.weatherapi25.weather import Weather


class WeatherInfo:

    WEATHER_CODE_ICON_MAP = {
        '200': fa.icons['bolt'],                 # thunderstorm
        '300': fa.icons['cloud-rain'],           # drizzle
        '500': fa.icons['cloud-showers-heavy'],  # rain
        '600': fa.icons['snowflake'],            # snow
        '700': fa.icons['smog'],                 # fog/low visibility
        '800': fa.icons['sun'],                  # clear
        '801': fa.icons['cloud-sun'],            # clouds
        '802': fa.icons['cloud-sun'],            # clouds
        '803': fa.icons['cloud'],                # lots of clouds
        '804': fa.icons['cloud'],                # lots of clouds
    }

    COLD_THRESHOLD = 10

    def __init__(self):
        self.weather_code = ''
        self.forecast_code = ''
        self.weather_status = ''
        self.forecast_status = ''
        self.updated_at = ''
        self.temperature = 0
        self.forecast_temperature = 0
        self.location = ''

    def get_temperature(self) -> int:
        return self.temperature

    def set_temperature(self, temperature: float) -> None:
        self.temperature = round(temperature)

    def get_forecast_temperature(self) -> int:
        return self.forecast_temperature

    def set_forecast_temperature(self, temperature: float):
        self.forecast_temperature = round(temperature)

    def get_formatted_location(self) -> str:
        return self.location.split(',')[0].title()

    def get_weather_icon(self) -> str:
        return self.__weather_code_to_icon(self.weather_code)

    def get_forecast_icon(self) -> str:
        return self.__weather_code_to_icon(self.forecast_code)

    def get_temperature_icon(self) -> str:
        return self.__temperature_to_icon(self.temperature)

    def get_forecast_temperature_icon(self) -> str:
        return self.__temperature_to_icon(self.forecast_temperature)

    def __temperature_to_icon(self, temperature: int) -> str:
        temperature_icon = fa.icons['temperature-high'] + '  '
        if temperature <= self.COLD_THRESHOLD:
            temperature_icon = fa.icons['temperature-low'] + '  '

        return temperature_icon

    def __weather_code_to_icon(self, code: int) -> str:
        weather_code = str(code)
        weather_code_clamped = weather_code[0] + '00'

        weather_icon = ''
        if weather_code_clamped in self.WEATHER_CODE_ICON_MAP:
            weather_icon = self.WEATHER_CODE_ICON_MAP[weather_code_clamped] \
                + '  '

            if weather_code in self.WEATHER_CODE_ICON_MAP:
                weather_icon = self.WEATHER_CODE_ICON_MAP[weather_code] + '  '

        return weather_icon

    def __format_weather_string(self, template: str) -> str:
        return template.format(
                self.get_formatted_location(), self.get_weather_icon(),
                self.weather_status, self.get_temperature_icon(),
                self.get_temperature(), self.get_forecast_icon(),
                self.forecast_status, self.get_forecast_temperature_icon(),
                self.get_forecast_temperature()
        )

    def get_short_string(self) -> str:
        template = '\n{}: {}{}, {}{}°C, next hour: {}{}, {}{}°C\n'
        return self.__format_weather_string(template)

    def get_long_string(self) -> str:
        template = ('\nWeather in {}\nNow: {}{}, with a temperature of {}{}°C'
                    '\nIn an hour: {}{} and {}{}°C\n')
        return self.__format_weather_string(template)

    def is_cache_stale(self, interval: int) -> bool:
        return (int(time()) - (interval * 60)) > self.updated_at

    def is_same_location(self, location: str) -> bool:
        return location.lower() == self.location.lower() or location == 'auto'

    @staticmethod
    def from_owm_data(location: str, weather: Weather, forecast: Weather):
        info = WeatherInfo()
        info.weather_code = weather.weather_code
        info.forecast_code = forecast.weather_code
        info.weather_status = weather.detailed_status
        info.forecast_status = forecast.detailed_status
        info.updated_at = int(time())
        info.location = location
        temp = weather.temperature(unit='celsius')['temp']
        info.set_temperature(temp)
        info.set_forecast_temperature(
                forecast.temperature(unit='celsius')['temp']
        )
        return info

    @staticmethod
    def deserialize(path: str):
        try:
            with open(path, 'r') as f:
                data = json.load(f)
                info = WeatherInfo()
                info.__dict__ = data
                return info
        except Exception:
            return None

    def serialize(self, path: str) -> None:
        directory = path.rsplit(os.sep, 1)[0]
        if not os.path.exists(directory):
            os.makedirs(directory)

        with open(path, 'w') as f:
            json.dump(self.__dict__, f, sort_keys=True)
