# This file is part of Weatherline.
#
# Weatherline is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Weatherline is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Weatherline.  If not, see <https://www.gnu.org/licenses/>.

from typing import Dict
import urllib.request as request
from urllib.error import URLError
import json


class IpApi:
    API_URL = 'http://ip-api.com/json/?' \
            'fields=status,message,countryCode,city,query'

    @staticmethod
    def get_geolocation() -> Dict:
        try:
            with request.urlopen(IpApi.API_URL) as response:
                content = response.read()
                data = json.loads(content)

                if 'city' not in data or 'countryCode' not in data:
                    raise RuntimeError('Invalid data received from ip-api.com')

                return data
        except URLError:
            raise RuntimeError('Unable to get data from ip-api.com')
