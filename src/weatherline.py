#!/usr/bin/env python3

# This file is part of Weatherline.
#
# Weatherline is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Weatherline is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Weatherline.  If not, see <https://www.gnu.org/licenses/>.

from argparse import ArgumentParser

from pyowm import OWM
from pyowm.utils import timestamps
from pyowm.commons.exceptions import UnauthorizedError
from pyowm.commons.exceptions import NotFoundError

from xdg import XDG_CACHE_HOME
import os

from weather_info import WeatherInfo
from ip_api import IpApi

CACHE_LOCATION = os.path.join(XDG_CACHE_HOME, 'weatherline', 'data.json')


def cached_data_exists() -> bool:
    return os.path.exists(CACHE_LOCATION)


def print_and_exit(weather_info: WeatherInfo, long_format: bool) -> int:
    if long_format:
        print(weather_info.get_long_string())
        return 0

    print(weather_info.get_short_string())
    return 0


def load_api_key_if_file(key: str) -> str:
    if os.path.exists(key):
        with open(key, 'r') as f:
            return f.readline().strip()

    return key


def main() -> int:
    parser = ArgumentParser(
        description='Display a short line with weather information'
    )
    parser.add_argument(
            '-k', '--api-key', required=True,
            help='OpenWeatherMap API key or a path pointing to a file with an'
                 ' API key.'
    )
    parser.add_argument(
            '-u', '--update-every', default='30',
            help='For how many minutes to cache weather data (default: 30)'
    )
    parser.add_argument(
            '-f', '--force', action='store_true',
            help='Forced update; skip cache and download new data'
    )
    parser.add_argument(
            '-l', '--long', action='store_true',
            help="Optional longer report format"
    )
    parser.add_argument(
            'location',
            help='Location in city,countrycode format where countrycode '
                 'is formatted according to ISO 3166. Countrycode is optional.'
                 ' Specify \'auto\' for autodetection using ip-api.com.'
    )

    args = parser.parse_args()
    api_key = load_api_key_if_file(args.api_key)
    location = args.location
    forced_update = args.force
    interval = int(args.update_every)
    long_format = args.long

    weather_info = None

    if cached_data_exists() and not forced_update:
        weather_info = WeatherInfo.deserialize(CACHE_LOCATION)
        if (weather_info is not None and
                not weather_info.is_cache_stale(interval) and
                weather_info.is_same_location(location)):
            return print_and_exit(weather_info, long_format)

    if location == 'auto':
        try:
            auto_location = IpApi.get_geolocation()
            location = auto_location['city'] + ',' \
                + auto_location['countryCode']
        except RuntimeError as e:
            print('Unable to determine location by ip: ' + str(e))
            return 1

    owm = OWM(api_key)
    manager = owm.weather_manager()

    try:
        weather = manager.weather_at_place(location).weather
    except UnauthorizedError:
        print('Invalid OpenWeatherMap API key')
        return 1
    except NotFoundError:
        print('City/place not found')
        return 1

    forecast_manager = manager.forecast_at_place(location, '3h')
    next_hour = timestamps.next_hour()
    forecast = forecast_manager.get_weather_at(next_hour)

    weather_info = WeatherInfo.from_owm_data(location, weather, forecast)
    weather_info.serialize(CACHE_LOCATION)
    return print_and_exit(weather_info, long_format)


if __name__ == "__main__":
    exit(main())
