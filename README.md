# Weatherline

A simple program outputting the weather in a single line. It reminds me to go
outside every once in a while.

![Screenshot](screenshot.png)

I know [ansiweather](https://github.com/fcambus/ansiweather) exists, but I
didn't like the default Unicode icons. At first I tried to use Emoji, but failed
miserably (printing complicated, colored Unicode characters to a terminal was
harder than I expected). Right now it uses FontAwesome for weather icons, but
I'd very much like to change it to something prettier.

## Running

Make sure pipenv and FontAwesome are installed. To run:
```sh
$ git clone
$ cd weatherline
$ pipenv install
$ pipenv run weatherline.py # or pipenv shell
```

## Usage

Get an [OpenWeatherMap API key](https://openweathermap.org/api). Then, get a
weather report by running ```weatherline.py -k <api key> <city>```. It is also
possible to store the API key in a file and use ```weatherline.py -k
/path/to/key <city>```. For more info, check ```weatherline.py --help```

## License
The code in this repository is licensed under 
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

The project icon is from [FontAwesome](https://fontawesome.com/license)
